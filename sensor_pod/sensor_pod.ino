
/**
 *  Sensor Pod code for RVOS
 *
 *  Written by Gerard Hickey <hickey@kinetic-compute.com>
 *
 */

#include <SPI.h>
#include <EEPROM.h>



const byte EE_initialized   = 0b10010101;

const byte POD_type         = 1;
const byte POD_version      = 1;

const byte CMD_read_dig     = 0b000;
const byte CMD_int_dig      = 0b001;
const byte CMD_write_dig    = 0b010;
const byte CMD_set_pwm      = 0b011;
const byte CMD_read_analog  = 0b100;
const byte CMD_cal_analog   = 0b101;
const byte CMD_identify     = 0b111;

const int num_samples = 10;


struct analog_cal {
  int low   = 0;
  int high  = 1023;
} calibrations[8];

struct analog_readings {
  int data[num_samples];
} analog_data[8];
int rotor = 0;
int analog_smoothed[8];

int analog_input[8] = {A0, A1, A2, A3, A4, A5, A6, A7};
int digital_pins[8] = {0, 1, 2, 3, 4, 5, 6, 7};

void setup() {
  // First byte of EEPROM tells us if it is initialized or not
  byte ee_status;
  EEPROM.get(0, ee_status);
  if (ee_status == EE_initialized) {
    load_analog_calibrations();
  } else {
    init_analog_calibrations();
    EEPROM.put(0, EE_initialized);
  }

  // Seed the analog readings.
  for (int i=0; i<8; i++) {
    int current = analogRead(analog_input[i]);
    for (int j=0; j<num_samples; j++) {
      analog_data[i].data[j] = current; 
    }
  }

  // have to send on master in, *slave out*
  pinMode(MISO, OUTPUT);

  // turn on SPI in slave mode
  SPCR |= bit (SPE);

  // now turn on interrupts
  SPI.attachInterrupt();
}

// SPI interrupt routine
ISR (SPI_STC_vect) {
  byte spi_cmd = SPDR;  // grab byte from SPI Data Register

  // identify parts of the command
  byte cmd = (spi_cmd & 0b11100000) >> 5;
  byte opt = (spi_cmd & 0b00011000) >> 3;
  byte io  = spi_cmd & 0b00000111;

  switch (cmd) {
    case CMD_read_dig:
      cmd_read_dig(io);
      break;

    case CMD_write_dig:
      break;

    case CMD_int_dig:
      break;

    case CMD_read_analog:
      break;

    case CMD_cal_analog:
      break;

    case CMD_set_pwm:
      break;

    case CMD_identify:
      cmd_identify();
      break; 

    default:  // all others ignored
      break;
  }
}


void loop() {
  // Update analog inputs
  for (int i=0; i<8; i++) {
    int value = analogRead(analog_input[i]);
    analog_data[i].data[rotor] = value;

    // Update the smoothed value
    int sum = 0;
    for (int j=0; j<num_samples; j++) {
      sum += analog_data[i].data[j];
    }
    analog_smoothed[i] = int(sum / num_samples);
  }

  // rotate rotor to next position
  rotor = rotor++ % 8;

  // delay some amount of time 
}


void init_analog_calibrations() {
  for (int i=0; i<8; i++) {
    calibrations[i].low = 0;
    calibrations[i].high = 1023;

    // Store calibration in EEPROM
    save_calibration(i, 0, 1023);
  }
}

void load_analog_calibrations() {
  int low, high;

  for (int i=0; i<8; i++) {
    load_calibration(i, &low, &high);
    calibrations[i].low = low;
    calibrations[i].high = high;
  }
}

void save_calibration(int io, int low, int high) {
  // Start of EEPROM +1 holds the calibration data
  // each calibration is 2 ints (4 bytes)
  int address = 1 + (io * 4);

  EEPROM.put(address, low);
  EEPROM.put(address+2, high);
}

void load_calibration(int io, int *low, int *high) {
  // Start of EEPROM +1 holds the calibration data
  // each calibration is 2 ints (4 bytes)
  int address = 1 + (io * 4);

  EEPROM.get(address, *low);
  EEPROM.get(address+2, *high);
}


void cmd_identify() {
  SPI.transfer(POD_type);
  SPI.transfer(POD_version);
}


void cmd_read_dig(byte io) {
  SPI.transfer(digitalRead(digital_pins[io]));
}

